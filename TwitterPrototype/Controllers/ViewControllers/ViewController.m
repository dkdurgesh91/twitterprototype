//
//  ViewController.m
//  TwitterPrototype
//
//  Created by Durgesh on 05/12/16.
//  Copyright © 2016 Craterzone. All rights reserved.
//

#import "ViewController.h"
#import "TblCellWithImage.h"
#import "TblCellWithoutImage.h"
#import "CustomTabBarCell.h"
#import "Constants.h"
#import "AppController.h"
#import "TwitterData.h"
#import "AppUtil.h"
#import "UIImageView+WebCache.h"
#import "InternetUtility.h"

@interface ViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    TwitterData *twitterData;
    NSMutableArray *updatedHashTags;
    BOOL isInitiate;
}
@property (weak, nonatomic) IBOutlet UINavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet UINavigationItem *navigationBarTitle;
@property (weak, nonatomic) IBOutlet UITableView *tweeterDataTableView;
@property (weak, nonatomic) IBOutlet UICollectionView *tabBarCollectionView;
@property (strong,nonatomic) UIRefreshControl *refreshControl;
@property (strong,nonatomic) UIActivityIndicatorView *activityIndicater;
@end

@implementation ViewController

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    return self;
}
#pragma mark -View life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    isInitiate = FALSE;
    updatedHashTags = [NSMutableArray arrayWithArray:self.hashTags];

    // Do any additional setup after loading the view, typically from a nib.
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self setTitleBar];
    if (!isInitiate) {
        [self getTweetData:[self callHashTags]];
        [self setDelegates];
        [self setPullToRefreshController];
        [self setActivityIndicater];
        isInitiate = TRUE;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark -View Setup

-(void)setTitleBar{
    _navigationBarTitle.title = [NSString stringWithFormat:NSLocalizedString(@"Group_Chat", nil),_name];
}

-(void)setDelegates{
    _tweeterDataTableView.estimatedRowHeight = 160.0f;
    _tweeterDataTableView.rowHeight = UITableViewAutomaticDimension;
    _tweeterDataTableView.delegate = self;
    _tweeterDataTableView.dataSource = self;
    _tweeterDataTableView.contentInset = UIEdgeInsetsZero;
}
-(void)setActivityIndicater{
    _activityIndicater = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    _activityIndicater.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    [_activityIndicater stopAnimating];
    _tweeterDataTableView.tableFooterView = _activityIndicater;
}
// Setup for pull to refresh
-(void)setPullToRefreshController{
    
    _refreshControl = [[UIRefreshControl alloc] init];
    _refreshControl.backgroundColor = [UIColor whiteColor];
    _refreshControl.tintColor = [UIColor blackColor];
    [_refreshControl addTarget:self
                        action:@selector(getLatestTweet)
              forControlEvents:UIControlEventValueChanged];
    [self.tweeterDataTableView addSubview:_refreshControl];
}

-(NSString*)callHashTags{
    
    NSInteger randomNumber= [AppUtil genrateRandomNumber:updatedHashTags.count];
    NSString *tag = [updatedHashTags objectAtIndex:randomNumber];
    [updatedHashTags removeObjectAtIndex:randomNumber];
    return tag;
}

-(void)getTweetData :(NSString *)name{
    if ([InternetUtility testInternetConnection]) {
        
        [[AppController sharedInstance] getTweetDataname:name :nil responseBlock:^(id response, NSInteger statusCode) {
            
            twitterData = [[TwitterData alloc ] initWithString:response error:nil];
            NSLog(@"%@",twitterData);
            [self reloadTableData];
            if (twitterData) {
                NSIndexPath *topPath = [NSIndexPath indexPathForRow:0 inSection:0];
                [_tweeterDataTableView scrollToRowAtIndexPath:topPath
                                             atScrollPosition:UITableViewScrollPositionTop
                                                     animated:YES];
            }
        }];
    }else{
        [AppUtil showAlertView:NSLocalizedString(@"Check_internetConnection", nil) :NSLocalizedString(@"Error", nil)];
    }
    
}

//  Refresh tweet list
-(void)getLatestTweet{
    [updatedHashTags removeAllObjects];
    updatedHashTags = [NSMutableArray arrayWithArray:self.hashTags];
    [self getTweetData:[self callHashTags]];

    if (self.refreshControl) {
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM d, h:mm a"];
        NSString *title = [NSString stringWithFormat:NSLocalizedString(@"Last_update",nil), [formatter stringFromDate:[NSDate date]]];
        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor blackColor]
                                                                    forKey:NSForegroundColorAttributeName];
        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
        _refreshControl.attributedTitle = attributedTitle;
        [_activityIndicater stopAnimating];
        [_refreshControl endRefreshing];
    }
    
}

-(void)reloadTableData
{
    [_tweeterDataTableView reloadData];
}



#pragma mark -Table view Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (twitterData) {
        _tweeterDataTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        return 1;
    }else{
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        
        messageLabel.text = NSLocalizedString(@"Loading", nil);
        messageLabel.textColor = [UIColor blackColor];
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = NSTextAlignmentCenter;
        messageLabel.font = [UIFont fontWithName:@"Palatino-Italic" size:20];
        [messageLabel sizeToFit];
        
        _tweeterDataTableView.backgroundView = messageLabel;
        _tweeterDataTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
    }
    return 0;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return twitterData.statuses.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Statuses *status = [twitterData.statuses objectAtIndex:indexPath.row];
    
    NSURL *avtorImageUrl = [NSURL URLWithString:status.user.profile_image_url_https];
    
    if (status.entities.media) {
        TblCellWithImage *cell = [tableView dequeueReusableCellWithIdentifier:@"CellWithImage"];
        if (cell == nil) {
            cell = [[TblCellWithImage alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellWithImage"];
        }
        cell.nameLabel.text = status.user.screen_name;
        [cell.avtorImage sd_setImageWithURL:avtorImageUrl placeholderImage:[UIImage imageNamed:@"AvtorImage"]];
        
        cell.timeLabel.text = [AppUtil getTimeDiffrence:status.created_at];
        cell.tweetDataTextView.text = status.text;
        
        Statuses *status = [twitterData.statuses objectAtIndex:indexPath.row];
        NSString *mediaImageName =[status.entities.media objectAtIndex:0].media_url_https;
        [cell.tweetImage sd_setImageWithURL:[NSURL URLWithString:mediaImageName]];
        
        return cell;
    }else{
        TblCellWithoutImage *cell = [tableView dequeueReusableCellWithIdentifier:@"CellWithoutImage"];
        if (cell == nil) {
            cell = [[TblCellWithoutImage alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellWithoutImage"];
        }
        cell.nameLabel.text =  status.user.screen_name;
        cell.timeLabel.text = [AppUtil getTimeDiffrence:status.created_at];
        cell.tweetDataTextView.text = status.text;
        [cell.avtorImage sd_setImageWithURL:avtorImageUrl placeholderImage:[UIImage imageNamed:@"AvtorImage"]];
        
        return cell;
    }
}


// Call lazy loading for more tweet
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    CGPoint translation = [scrollView.panGestureRecognizer translationInView:scrollView.superview];
    
    if ([scrollView isKindOfClass:[UITableView class]]) {
        if (translation.y <= 0 ){
            float bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height;
            if(bottomEdge >= (scrollView.contentSize.height-200))
            {
                if ([InternetUtility testInternetConnection]) {
                    
                    if(twitterData.search_metadata.next_results) {
                        NSLog(@"LazyLoading");
                        [self lazyLoading];
                    }else if (updatedHashTags.count > 0){
                        NSLog(@"Call another tag");
                        [self callOtherHashTag:[self callHashTags]];
                    }
                    
                }else{
                    [AppUtil showAlertView:NSLocalizedString(@"Check_internetConnection", nil) :NSLocalizedString(@"Error", nil)];
                }
            }
        }
    }
}

// LazyLoading webService
-(void)lazyLoading{
    [_activityIndicater startAnimating];
    [[AppController sharedInstance] lazyLodingOfTwitterData:twitterData.search_metadata.next_results responseBlock:^(id response, NSInteger statusCode) {
        TwitterData *LazyLodingData = [[TwitterData alloc ] initWithString:response error:nil];
        NSLog(@"Tweet data:  %@",LazyLodingData);
        [self addLazyLodingDatainToPreviouce:LazyLodingData];
    }];

}

//Call another hashtag data

-(void)callOtherHashTag:(NSString *)tagName
{
    [_activityIndicater startAnimating];
    [[AppController sharedInstance] getTweetDataname:tagName :nil responseBlock:^(id response, NSInteger statusCode) {
        
        TwitterData *newTagData = [[TwitterData alloc ] initWithString:response error:nil];
        NSLog(@"%@",newTagData);
        [self addLazyLodingDatainToPreviouce:newTagData];
    }];
}

// Add latest fetched data in tweet data
-(void)addLazyLodingDatainToPreviouce :(TwitterData*)newData
{
    if (newData) {
        twitterData.search_metadata = newData.search_metadata;
        [twitterData.statuses addObjectsFromArray:newData.statuses];
        [self reloadTableData];
    }
    [_activityIndicater stopAnimating];
}




@end
