//
//  AppController.m
//  TwitterPrototype
//
//  Created by Durgesh on 05/12/16.
//  Copyright © 2016 Craterzone. All rights reserved.
//

#import "AppController.h"
#import "WebServiceHandler.h"

@implementation AppController

+(AppController *)sharedInstance {
    static AppController *sharedSingleton;
    @synchronized(self)
    {
        if (!sharedSingleton) {
            sharedSingleton = [[AppController alloc] init];
            
        }
        return sharedSingleton;
    }
}

-(void)getAuthToken:(NSDictionary *)request responseBlock:(ResponseBlock)block
{
    [[WebServiceHandler sharedInstance] getData:^(id response, NSInteger statusCode) {
        
        if (block) {
            block(response,statusCode);
        }
    }];
}

-(void)getTweetDataname:(NSString*)name :(NSDictionary *)request responseBlock:(ResponseBlock)block
{
[[WebServiceHandler sharedInstance] getTweet:name :^(id response, NSInteger statusCode) {
    if (block) {
        block(response,statusCode);
    }
}];
}

-(void)lazyLodingOfTwitterData:(NSString*)nextResult  responseBlock:(ResponseBlock)block
{
[[WebServiceHandler sharedInstance] lazyLodingOfTwitterData:nextResult responseBlock:^(id response, NSInteger statusCode) {
    if (block) {
        block(response,statusCode);
    }
}];
}


@end
