//
//  AppController.h
//  TwitterPrototype
//
//  Created by Durgesh on 05/12/16.
//  Copyright © 2016 Craterzone. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef void (^ResponseBlock)(id response, NSInteger statusCode);

@interface AppController : NSObject
+(AppController *)sharedInstance;


-(void)getAuthToken:(NSDictionary *)request responseBlock:(ResponseBlock)block;
-(void)getTweetDataname:(NSString*)name :(NSDictionary*)request responseBlock:(ResponseBlock)block;
-(void)lazyLodingOfTwitterData:(NSString*)nextResult  responseBlock:(ResponseBlock)block;
@end
