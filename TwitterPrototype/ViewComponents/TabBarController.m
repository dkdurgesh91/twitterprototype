//
//  TabBarController.m
//  TwitterPrototype
//
//  Created by Durgesh on 08/12/16.
//  Copyright © 2016 Craterzone. All rights reserved.
//

#import "TabBarController.h"
#import "ViewController.h"

@interface TabBarController ()
@property (strong, nonatomic) IBOutlet UITabBar *tabBar;

@end

@implementation TabBarController
@synthesize tabBar;
- (void)viewDidLoad {
    [super viewDidLoad];
    
//    UIViewController *view1 = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
//    UIViewController *view2 = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
//    UIViewController *view3 = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
//    
//    NSMutableArray *tabViewControllers = [[NSMutableArray alloc] init];
//    [tabViewControllers addObject:view1];
//    [tabViewControllers addObject:view2];
//    [tabViewControllers addObject:view3];
//    
//    self.tabBarController.viewControllers = tabViewControllers;
//    //[self.tabBar setViewControllers:tabViewControllers];
//    //can't set this until after its added to the tab bar
//    view1.tabBarItem =
//    [[UITabBarItem alloc] initWithTitle:@"view1"
//                                  image:[UIImage imageNamed:@"view1"]
//                                    tag:1];
//    view2.tabBarItem =
//    [[UITabBarItem alloc] initWithTitle:@"view2"
//                                  image:[UIImage imageNamed:@"view3"]
//                                    tag:2];
//    view3.tabBarItem =
//    [[UITabBarItem alloc] initWithTitle:@"view3"
//                                  image:[UIImage imageNamed:@"view3"]
//                                    tag:3];
//    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
