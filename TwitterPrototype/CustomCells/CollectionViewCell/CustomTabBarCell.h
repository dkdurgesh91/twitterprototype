//
//  CustomTabBarCell.h
//  TwitterPrototype
//
//  Created by Durgesh on 05/12/16.
//  Copyright © 2016 Craterzone. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTabBarCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end
