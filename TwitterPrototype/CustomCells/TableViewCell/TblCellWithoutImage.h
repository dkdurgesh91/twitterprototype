//
//  TblCellWithoutImage.h
//  TwitterPrototype
//
//  Created by Durgesh on 05/12/16.
//  Copyright © 2016 Craterzone. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TblCellWithoutImage : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *avtorImage;
@property (weak, nonatomic) IBOutlet UIView *nameView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIView *tweetDetailView;
@property (weak, nonatomic) IBOutlet UITextView *tweetDataTextView;

@end
