//
//  TblCellWithImage.m
//  TwitterPrototype
//
//  Created by Durgesh on 05/12/16.
//  Copyright © 2016 Craterzone. All rights reserved.
//

#import "TblCellWithImage.h"

@implementation TblCellWithImage

- (void)awakeFromNib {
    [super awakeFromNib];
    
    _avtorImage.layer.cornerRadius = _avtorImage.frame.size.width/2;
    _avtorImage.layer.masksToBounds = YES;

    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
