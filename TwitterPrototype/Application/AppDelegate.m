//
//  AppDelegate.m
//  TwitterPrototype
//
//  Created by Durgesh on 05/12/16.
//  Copyright © 2016 Craterzone. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import "Constants.h"

@interface AppDelegate (){

}
@end

@implementation AppDelegate
@synthesize tabBarController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [self setTabBars];
    
    // Override point for customization after application launch.
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(void)setTabBars {
    tabBarController = [self setTabBar];
    self.navController = [[UINavigationController alloc] initWithRootViewController:tabBarController];
    
    self.window = [[UIWindow alloc]initWithFrame:CGRectMake(0, 0,
                                                            [UIScreen mainScreen].bounds.size.width,
                                                            [UIScreen mainScreen].bounds.size.height
                                                            )];
    
    self.navController.navigationBarHidden = YES;
    self.window.rootViewController = self.navController;
}

-(UITabBarController *)setTabBar {
    UITabBarController *tabBarController1 = [[UITabBarController alloc] init];
    tabBarController1.tabBar.translucent = NO;
    tabBarController1.tabBar.barStyle = UIBarStyleBlack;
    [[UITabBar appearance] setBarTintColor:[UIColor colorWithRed:166/255.0f green:250/255.0f blue:250/255.0f alpha:1]];
    [[UITabBar appearance] setAlpha:1.0];
    
    
    // ------------------------------
    // Mapping View Controllers with tabs
    // ------------------------------
    
    NSMutableArray * tabArray = [[NSMutableArray alloc] init];
    ViewController *viewController;
    
    /*---------------Diet Tab--------------------*/
    viewController = [self createTabViewController];
    viewController.name = NSLocalizedString(@"Diet", nil);
    viewController.hashTags = @[@"Weightloss",@"diet",@"reducing",@"detox",@"nutrition",@"superfood"];
    [tabArray insertObject:viewController atIndex:0];
    
    /*------------------ Skin whitening tab------------------*/
    viewController = [self createTabViewController];
    viewController.name = NSLocalizedString(@"Fairness", nil);
    viewController.hashTags = @[@"skinwhitening",@"fairness",@"fairnesscream"];
    [tabArray insertObject:viewController atIndex:1];
    

    
    /*-------------- Demonetization tab----------------------*/
    viewController = [self createTabViewController];
    viewController.name = NSLocalizedString(@"Demonetization", nil);
    viewController.hashTags = @[@"demonetisation",@"demonetization",@"demonet",@"Blackmoney",@"currencyBan",@"notesban",@"cashlessEconomy"];
    [tabArray insertObject:viewController atIndex:2];
    
    
    /*----------------Mumbai Tab-----------------------*/
    viewController = [self createTabViewController];
    viewController.name = NSLocalizedString(@"Mumbai", nil);
    viewController.hashTags = @[@"Mumbai",@"Bombay",@"mymumbai",@"mumbaikar"];
    [tabArray insertObject:viewController atIndex:3];
    
    tabBarController1.viewControllers = tabArray;
    
    // ------------------------
    // Setting Tabs Text
    // ------------------------
    [self setTabBarTitle:[[tabBarController1.tabBar items] objectAtIndex:0] :NSLocalizedString(@"Diet", nil)];
    [self setTabBarTitle:[[tabBarController1.tabBar items] objectAtIndex:1] :NSLocalizedString(@"Fairness", nil)];
    [self setTabBarTitle:[[tabBarController1.tabBar items] objectAtIndex:2] :NSLocalizedString(@"Demonet", nil)];
    [self setTabBarTitle:[[tabBarController1.tabBar items] objectAtIndex:3] :NSLocalizedString(@"Mumbai", nil)];

    
    tabBarController1.delegate = self;
    tabBarController1.selectedIndex = 0;
    
    return tabBarController1;
}
-(ViewController*)createTabViewController{
    return [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ViewController"];
}

// Set Tab Bar title
-(void)setTabBarTitle:(UITabBarItem*)tabBarItem :(NSString*)title
{
    tabBarItem.title = title;
     tabBarItem.titlePositionAdjustment = UIOffsetMake(0, -13);
        
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue-Bold" size:17.0f],
                                                       NSForegroundColorAttributeName : SELECTED_COLOR
                                                       } forState:UIControlStateSelected];
    
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue-Bold" size:17.0f],
                                                        NSForegroundColorAttributeName : DESELECTED_COLOR
                                                        } forState:UIControlStateNormal];
    
}

@end
