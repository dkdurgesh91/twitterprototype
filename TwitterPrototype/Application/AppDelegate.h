//
//  AppDelegate.h
//  TwitterPrototype
//
//  Created by Durgesh on 05/12/16.
//  Copyright © 2016 Craterzone. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate,UITabBarControllerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) UITabBarController *tabBarController;
@property (nonatomic, strong) UINavigationController *navController;



@end

