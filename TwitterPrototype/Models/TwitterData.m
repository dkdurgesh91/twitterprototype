//
//  TwitterData.m
//  TwitterPrototype
//
//  Created by Durgesh on 06/12/16.
//  Copyright © 2016 Craterzone. All rights reserved.
//

#import "TwitterData.h"

@implementation Media

-(id)initWithDictionary:(NSDictionary *)dict error:(NSError *__autoreleasing *)err {
    
    self = [super initWithDictionary:dict error:err];
    
    return self;
}
@end

@implementation Entities
-(id)initWithDictionary:(NSDictionary *)dict error:(NSError *__autoreleasing *)err {
    
    self = [super initWithDictionary:dict error:err];
    self.media = (NSMutableArray <Optional>*)[Media arrayOfModelsFromDictionaries:[dict valueForKey:@"media"] error:nil];
    
    return self;
}

@end


@implementation User


@end

@implementation Statuses

-(id)initWithDictionary:(NSDictionary *)dict error:(NSError *__autoreleasing *)err {
    
    self = [super initWithDictionary:dict error:err];
    
    return self;
}

-(void)setText:(NSString<Optional> *)text
{
    text = [self removeHashFromString:text];
    _text = [self trimString:text];
}
// Remove # from tweet text
-(NSString *)removeHashFromString:(NSString*)text{
  return [text stringByReplacingOccurrencesOfString:@"#" withString:@""];
}
//Remove "RT @....:" pattern and place with "FWD:" in text
-(NSString *)trimString:(NSString *)string{
    NSMutableString *newString = [NSMutableString stringWithString:string];
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern: @"RT @(.*):" options:0 error:nil];
    [regex replaceMatchesInString:newString options:0 range:NSMakeRange(0, [newString length]) withTemplate:@"FWD:"];
    return newString;
}

@end
@implementation Search_metadata

@end

@implementation TwitterData
-(id)initWithDictionary:(NSDictionary *)dict error:(NSError *__autoreleasing *)err {
    
    self = [super initWithDictionary:dict error:err];
    self.statuses = (NSMutableArray <Optional>*)[Statuses arrayOfModelsFromDictionaries:[dict valueForKey:@"statuses"] error:nil];
    
    return self;
}

@end

@implementation FullTwitterData


@end
