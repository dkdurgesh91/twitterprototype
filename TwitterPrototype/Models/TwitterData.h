//
//  TwitterData.h
//  TwitterPrototype
//
//  Created by Durgesh on 06/12/16.
//  Copyright © 2016 Craterzone. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface Media : JSONModel
@property (nonatomic,strong) NSString<Optional> *media_url_https;
-(id)initWithDictionary:(NSDictionary *)dict error:(NSError *__autoreleasing *)err ;
@end

@interface Entities : JSONModel
@property (nonatomic,strong)NSMutableArray <Media*> *media;
-(id)initWithDictionary:(NSDictionary *)dict error:(NSError *__autoreleasing *)err;
@end

@interface User : JSONModel
@property (nonatomic,strong) NSString<Optional> *profile_image_url_https;
@property (nonatomic,strong) NSString<Optional> *screen_name;
@end

@interface Statuses : JSONModel
@property (nonatomic,strong) NSString<Optional> *created_at;
@property (nonatomic,strong) NSString<Optional> *text;
@property (nonatomic,strong) User<Optional> *user;
@property (nonatomic,strong) Entities<Optional> *entities;
-(id)initWithDictionary:(NSDictionary *)dict error:(NSError *__autoreleasing *)err;


@end

@interface Search_metadata : JSONModel
@property (nonatomic,strong)NSString<Optional> *next_results;
@property (nonatomic,strong)NSString<Optional> *refresh_url;
@end

@interface TwitterData : JSONModel
@property (strong,nonatomic)NSMutableArray <Statuses *> *statuses;
@property (strong,nonatomic)Search_metadata *search_metadata;
-(id)initWithDictionary:(NSDictionary *)dict error:(NSError *__autoreleasing *)err;

@end




@interface FullTwitterData : JSONModel
@property (strong, nonatomic) NSMutableArray <TwitterData *> *object;
@end

