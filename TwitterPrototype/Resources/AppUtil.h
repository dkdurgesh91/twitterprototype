//
//  AppUtil.h
//  TwitterPrototype
//
//  Created by Durgesh on 05/12/16.
//  Copyright © 2016 Craterzone. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppUtil : NSObject
+(AppUtil *)sharedInstance;
+(NSString *)getTimeDiffrence :(NSString *)created_at;
+(void)showAlertView :(NSString *)alertMessage :(NSString *)msgTitle;
+(NSInteger)genrateRandomNumber:(NSInteger)max;
@end
