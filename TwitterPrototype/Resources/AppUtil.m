//
//  AppUtil.m
//  TwitterPrototype
//
//  Created by Durgesh on 05/12/16.
//  Copyright © 2016 Craterzone. All rights reserved.
//

#import "AppUtil.h"
#import "Constants.h"
#import <UIKit/UIKit.h>

@implementation AppUtil

+(AppUtil *)sharedInstance {
    static AppUtil *sharedSingleton;
    @synchronized(self)
    {
        if (!sharedSingleton) {
            sharedSingleton = [[AppUtil alloc] init];
            
        }
        return sharedSingleton;
    }
}

//Create a random number
+(NSInteger)genrateRandomNumber:(NSInteger)max{
    int randNum = rand() % (max - 0) + 0; //create the random number.
    return randNum;
}


// Get time diffrence between Tweet and current time
+(NSString *)getTimeDiffrence :(NSString *)created_at
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"EEE, MMM d HH:mm:ss ZZZ yyyy"];
    NSDate *date = [dateFormat dateFromString:created_at];
    NSDateComponentsFormatter *formatter = [[NSDateComponentsFormatter alloc] init];
    formatter.unitsStyle = NSDateComponentsFormatterUnitsStyleAbbreviated;
    formatter.allowedUnits = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    NSString *elapsed = [formatter stringFromDate:date toDate:[NSDate date]];
    
    return elapsed;
}

// Show alert view
+(void)showAlertView :(NSString *)alertMessage :(NSString *)msgTitle {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:msgTitle message:alertMessage delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", nil) otherButtonTitles:nil, nil];
    
    [alertView show];
}

@end
