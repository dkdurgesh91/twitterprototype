//
//  WebServiceLibrary.m
//  WebServiceLibrary
//
//  Created by Babul Prabhakar on 25/03/15.
//  Copyright (c) 2015 Babul Prabhakar. All rights reserved.
//

#import "HTTPClientUtil.h"
#import "AFHTTPRequestOperationManager.h"
#import "AppDelegate.h"

NSString* const CONTENT_TYPE_KEY = @"Content-Type";
NSString* const ACCEPTANCE_TYPE_KEY = @"Accept";
NSString* const USER_AGENT_KEY = @"user-agent";

@interface HTTPClientUtil() {
    
}

@end
@implementation HTTPClientUtil

NSString* const USER_AGENT = @"Mozilla/5.0 (X11; mios) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.91 ";


/*!
 * @author - Babul Prabhakar
 * post request to server and post response.
 * parameters : NSString(requestUrl), NSDictionary(JSON data to send), NSString(HTTP method = "POST")
 */
+(void)postDataToWS :(NSString *)requestUrl parameters:(NSData *)parameters WithHeaderDict:(NSDictionary *)headerDict withBlock:(RequestCompletionBlock)block   {
    
    if(requestUrl != nil) {
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        [manager.requestSerializer setValue:CONTENT_TYPE forHTTPHeaderField:CONTENT_TYPE_KEY];
        
        if (headerDict != nil) {
            [headerDict enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                [manager.requestSerializer setValue:(NSString *)obj  forHTTPHeaderField:key];
            }];
        }
        
        [manager POST:requestUrl parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            if (block) {
                block(operation,nil);
            }
            
        }
         
         
              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  
                  if (block) {
                      block(operation,error);
                  }
                  
              }];
    }
    
}




/**
 * author - Babul Prabhakar
 * get request to server and get response.
 * parameters : NSString(requestUrl), NSString(HTTP method = "GET")
 */
+(void)getDataFromWS:(NSString *)requestUrl WithHeaderDict:(NSDictionary *)headerDict  withBlock:(RequestCompletionBlock)block  {
    
    if(requestUrl != nil) {
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
                [manager.requestSerializer setValue:@"Bearer AAAAAAAAAAAAAAAAAAAAAMreyAAAAAAACCMyWd0ck5GNK%2FjlaCM8ttlwHMs%3D3Bm7MDhJf3QXTBiM49wDdHftwRlmBt9cbfHgrMVcrG82x0oLz7" forHTTPHeaderField:@"Authorization"];
        
        [manager GET:requestUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            if (block) {
                block(operation,nil);
            }
            
        }
             failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                 if (block) {
                     block(operation,error);
                 }
                 
             }];
    }
    
}

/*
 *@author - Babul Prabhakar
 *delete request to server
 *parameters : NSString(requestUrl), NSString(HTTP method = "DELTE")
 */
+(void)deleteFromWS:(NSString *)requestUrl parameters:(NSDictionary *)parameters WithHeaderDict:(NSDictionary *)headerDict withBlock:(RequestCompletionBlock)block {
    
    if(requestUrl != nil) {
        
       // NSString *userAgent = [HTTPClientUtil getUserAgent];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        [manager.requestSerializer setValue:CONTENT_TYPE forHTTPHeaderField:CONTENT_TYPE_KEY];
        [manager.requestSerializer setValue:ACCEPTANCE_DATA forHTTPHeaderField:ACCEPTANCE_TYPE_KEY];
        //[manager.requestSerializer setValue:userAgent forHTTPHeaderField:USER_AGENT_KEY];
        
        [headerDict enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            [manager.requestSerializer setValue:(NSString *)obj  forHTTPHeaderField:key];
        }];
        [manager DELETE:requestUrl parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             if (block) {
                 block(operation,nil);
             }
             
         }
                failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    
                    if (block) {
                        block(operation,error);
                    }
                    
                }];
    }
    
}


/*
 *@author - Babul Prabhakar
 *post request to server and get response.
 *parameters : NSString(requestUrl), NSDictionary(JSON data to send), NSString(HTTP method = "POST")
 */
+(void)postMultiPartToWS :(NSString *)requestUrl fileName:(NSString*) fileName  parameters:(NSDictionary *)parameters WithHeaderDict:(NSDictionary *)headerDict withBlock:(RequestCompletionBlock)block{
    
    if(requestUrl != nil && fileName!= nil ) {
        
        NSData *data = [parameters objectForKey:@"data"];
        NSString *exten = [parameters objectForKey:@"exten"];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        [manager.requestSerializer setValue:CONTENT_TYPE forHTTPHeaderField:CONTENT_TYPE_KEY];
        [manager.requestSerializer setValue:ACCEPTANCE_DATA forHTTPHeaderField:ACCEPTANCE_TYPE_KEY];
        [manager POST:requestUrl parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData){
            [formData appendPartWithFileData:data name:@"file" fileName:fileName mimeType:exten];
        }success:^(AFHTTPRequestOperation *operation, id responseObject){
            if (block) {
                block(operation,nil);
            }
        }
         
              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  if (block) {
                      block(operation,error);
                  }
                  
              }];
        
    }
}


@end
