//
//  WebServices.h
//  TwitterPrototype
//
//  Created by Durgesh on 05/12/16.
//  Copyright © 2016 Craterzone. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WebServices : NSObject
+(NSString *)getbearerTokenURL;
+(NSString*)getTweetUrl :(NSString*)name;
+(NSString*)getLazyLoadingUrl :(NSString*)nextResult;

@end
