//
//  WebServices.m
//  TwitterPrototype
//
//  Created by Durgesh on 05/12/16.
//  Copyright © 2016 Craterzone. All rights reserved.
//

#import "WebServices.h"

@implementation WebServices

+(NSString *)getbearerTokenURL
{
return @"https://api.twitter.com/oauth2/token";
}

+(NSString*)getTweetUrl :(NSString*)name
{
    return [NSString stringWithFormat:@"https://api.twitter.com/1.1/search/tweets.json?tag=%@&rpp=1",name];
//return @"https://api.twitter.com/1.1/search/tweets.json?tag=bombay&mumbai&rpp=1";
}

+(NSString*)getLazyLoadingUrl :(NSString*)nextResult
{
    return [NSString stringWithFormat:@"https://api.twitter.com/1.1/search/tweets.json%@",nextResult];
}

@end
