//
//  WebServiceHandler.m
//  TwitterPrototype
//
//  Created by Durgesh on 05/12/16.
//  Copyright © 2016 Craterzone. All rights reserved.
//

#import "WebServiceHandler.h"
#import "AppUtil.h"
#import "WebServices.h"
#import "HTTPClientUtil.h"
#import "AFNetworking.h"


@implementation WebServiceHandler

+(WebServiceHandler *)sharedInstance {
    static WebServiceHandler *sharedSingleton;
    @synchronized(self)
    {
        if (!sharedSingleton) {
            sharedSingleton = [[WebServiceHandler alloc] init];
            
        }
        return sharedSingleton;
    }
}

// First initlizing data
-(void)getTweet :(NSString*)name :(ResponseBlock)responseBlock
{
    //Authorization key like-->  @{@"Authorization": @"805993548-XCLI5QDUCLRpxtwFNryniVlJYXBSbNqff9bfsaAQ"};

    NSDictionary *dict = @{kAUTHORIZATION: AUTHORIZATION_KEY};
    
    [HTTPClientUtil getDataFromWS:[WebServices getTweetUrl:name] WithHeaderDict:dict withBlock:^(AFHTTPRequestOperation *response, NSError *error) {
        if (responseBlock) {
            responseBlock(response.responseString,response.response.statusCode);
        }
    }];
}

// Lazy loading data
-(void)lazyLodingOfTwitterData:(NSString*)nextResult responseBlock:(ResponseBlock)responseBlock{
    
    //Authorization key like-->  @{@"Authorization": @"805993548-XCLI5QDUCLRpxtwFNryniVlJYXBSbNqff9bfsaAQ"};

    NSDictionary *dict = @{kAUTHORIZATION: AUTHORIZATION_KEY};
    
    [HTTPClientUtil getDataFromWS:[WebServices getLazyLoadingUrl:nextResult] WithHeaderDict:dict withBlock:^(AFHTTPRequestOperation *response, NSError *error) {
        if (responseBlock) {
            responseBlock(response.responseString,response.response.statusCode);
        }
    }];
    
}


@end
