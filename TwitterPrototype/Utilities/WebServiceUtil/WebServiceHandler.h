//
//  WebServiceHandler.h
//  TwitterPrototype
//
//  Created by Durgesh on 05/12/16.
//  Copyright © 2016 Craterzone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"

@interface WebServiceHandler : NSObject
+(WebServiceHandler *)sharedInstance;

-(NSDictionary *)getAuthHeaderDict;

-(void)getData:(ResponseBlock)responseBlock;

-(void)getTweet :(NSString*)name :(ResponseBlock)responseBlock;

-(void)lazyLodingOfTwitterData:(NSString*)nextResult responseBlock:(ResponseBlock)responseBlock;
@end
